import React from 'react';

import './src/i18n/texts';
import './src/styles/globals.css';
import './src/styles/block.css';
import './src/styles/font-face.css';
import './src/styles/grid.css';
import './src/styles/tailwind.css';
import './src/styles/typography.css';

import { Layout } from './src/components';

export function wrapPageElement({ element, props }) {
    return <Layout {...props}>{element}</Layout>;
}
