#!/bin/sh -e
PROJECT_ROOT="$(cd $(dirname $0)/..; pwd)"

cd $PROJECT_ROOT

rm -rf ./build;

gatsby build;

cp -a ./public/. ./build;
rm -rf ./public;

echo "Successfull build" 
