import React from 'react';
import './src/i18n/texts';

import { Layout } from './src/components';

export function wrapPageElement({ element, props }) {
    return <Layout {...props}>{element}</Layout>;
}
