import React from 'react';
import { BgImage } from 'gbimage-bridge';
import { getImage } from 'gatsby-plugin-image';
import classNames from 'classnames';

import * as s from './block.module.css';

export const Block = ({ children, background = 'black', bgImage, paddings = true, id }) => {
    return (
        <>
            {bgImage && (
                <BgImage image={getImage(bgImage)} loading="eager">
                    <div className={classNames(s.container, paddings && s.paddings)} id={id}>
                        {children}
                    </div>
                </BgImage>
            )}

            {!bgImage && (
                <div className={s[background]}>
                    <div className={classNames(s.container, paddings && s.paddings)} id={id}>
                        {children}
                    </div>
                </div>
            )}
        </>
    );
};
