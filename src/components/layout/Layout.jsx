import React from 'react';

import { Footer } from './Footer';
import { Header } from './header/Header';
import * as s from './layout.module.css';

export const Layout = ({ children }) => {
    return (
        <main className={s.container}>
            <Header />
            {children}
            <Footer />
        </main>
    );
};
