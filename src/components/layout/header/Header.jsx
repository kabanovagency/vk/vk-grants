import classNames from 'classnames';
import React, { useCallback, useState } from 'react';

import * as s from './header.module.css';
import * as menu from './header-menu.module.css';
import Logo from './Logo';

export const Header = () => {
    const scrollToNext = useCallback(
        id => () => {
            let el = document.getElementById(id);

            if (!el) return;

            el.scrollIntoView({
                behavior: 'smooth',
                block: 'start',
            });
        },
        [],
    );

    const [show, setMenu] = useState(false);

    const menuScrollToNext = useCallback(
        id => () => {
            setMenu(false);
            scrollToNext(id)();
        },
        [],
    );

    return (
        <div className={s.wrapper}>
            <div className={classNames(s.box, s.desktop)}>
                <Logo />
                <div className={s.anchors}>
                    <div className={s.anchor} onClick={scrollToNext('purpose')}>
                        о программе
                    </div>
                    <div className={s.anchor} onClick={scrollToNext('options')}>
                        какие есть гранты
                    </div>
                    <div className={s.anchor} onClick={scrollToNext('recieveing')}>
                        как получить грант
                    </div>
                    <div className={s.anchor}>подать заявку</div>
                </div>
            </div>

            {!show && (
                <div className={classNames(s.box, s.mobile)}>
                    <Logo />
                    <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        className={s.menu}
                        onClick={() => setMenu(true)}
                    >
                        <path
                            d="M21 11H3C2.44772 11 2 11.4477 2 12C2 12.5523 2.44772 13 3 13H21C21.5523 13 22 12.5523 22 12C22 11.4477 21.5523 11 21 11ZM21 4H3C2.44772 4 2 4.44772 2 5C2 5.55228 2.44772 6 3 6H21C21.5523 6 22 5.55228 22 5C22 4.44772 21.5523 4 21 4ZM3 20H21C21.5523 20 22 19.5523 22 19C22 18.4477 21.5523 18 21 18H3C2.44772 18 2 18.4477 2 19C2 19.5523 2.44772 20 3 20Z"
                            fill="white"
                        />
                    </svg>
                </div>
            )}

            {show && (
                <div className={menu.box}>
                    <div className={menu.close}>
                        <svg
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            onClick={() => setMenu(false)}
                        >
                            <path
                                d="M7.5364 6.2636C7.18492 5.91213 6.61508 5.91213 6.2636 6.2636C5.91213 6.61508 5.91213 7.18492 6.2636 7.5364L10.7272 12L6.2636 16.4636C5.91213 16.8151 5.91213 17.3849 6.2636 17.7364C6.61508 18.0879 7.18492 18.0879 7.5364 17.7364L12 13.2728L16.4636 17.7364C16.8151 18.0879 17.3849 18.0879 17.7364 17.7364C18.0879 17.3849 18.0879 16.8151 17.7364 16.4636L13.2728 12L17.7364 7.5364C18.0879 7.18492 18.0879 6.61508 17.7364 6.2636C17.3849 5.91213 16.8151 5.91213 16.4636 6.2636L12 10.7272L7.5364 6.2636Z"
                                fill="#99A2AD"
                            />
                        </svg>
                    </div>
                    <ul className={menu.list}>
                        <li className={menu.item}>
                            <span onClick={menuScrollToNext('purpose')}>о программе</span>
                        </li>
                        <li className={menu.item}>
                            <span onClick={menuScrollToNext('options')}>какие есть гранты</span>
                        </li>
                        <li className={menu.item}>
                            <span onClick={menuScrollToNext('recieveing')}>как получить грант</span>
                        </li>
                        <li className={menu.item}>
                            <span>подать заявку</span>
                        </li>
                    </ul>
                </div>
            )}
        </div>
    );
};
