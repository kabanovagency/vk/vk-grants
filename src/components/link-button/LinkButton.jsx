import React from 'react';

import * as s from './link-button.module.css';

export const LinkButton = ({ children }) => {
    return <a className={s.box}>{children}</a>;
};
