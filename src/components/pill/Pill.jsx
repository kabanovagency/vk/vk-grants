import classNames from 'classnames';
import React from 'react';

import * as s from './pill.module.css';

export const Pill = ({ children, color = 'white', onMouseOver }) => {
    return (
        <span data-pill onMouseOver={onMouseOver} className={classNames(s.item, s[color])}>
            {children}
        </span>
    );
};
