import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import ru from './ru.json';

i18n.use(initReactI18next).init({
    fallbackLng: 'ru',
    resources: {
        ru: {
            translation: ru,
        },
    },
});

i18n.changeLanguage('ru');
