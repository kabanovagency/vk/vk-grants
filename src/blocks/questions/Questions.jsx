import classNames from 'classnames';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Block } from '../../components';
import * as title from './questions-title.module.css';
import * as list from './questions-list.module.css';
import * as s from './questions.module.css';

const Item = ({ question, answer }) => {
    let { t } = useTranslation();
    let [show, setShow] = useState(false);
    return (
        <div className={list.item} onClick={() => setShow(prev => !prev)}>
            <div className={list.question}>
                <h3 className={classNames('h2', list.title)}>{t(question)}</h3>
                <svg
                    className={classNames(list.icon, show && list.rotated)}
                    width="30"
                    height="30"
                    viewBox="0 0 30 30"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path d="M15 0L15 30M0 15L30 15" stroke="white" strokeWidth="2" />
                </svg>
            </div>

            {show && <p className={classNames(list.answer, 'text')}>{t(answer)}</p>}
        </div>
    );
};

export const Questions = () => {
    let { t } = useTranslation();
    return (
        <Block background="gray">
            <h2 className={title.container}>
                <div className={classNames(title.line1, 'h1-vk')}>{t('questions-block.title.t1')}</div>
                <div className={classNames(title.line2, 'h1-tt')}>{t('questions-block.title.t2')}</div>
            </h2>

            <div className={s.questions}>
                {new Array(7).fill().map((_, i) => (
                    <Item
                        key={i}
                        question={`questions-block.questions.q${i + 1}.title`}
                        answer={`questions-block.questions.q${i + 1}.answer`}
                    />
                ))}
            </div>
        </Block>
    );
};
