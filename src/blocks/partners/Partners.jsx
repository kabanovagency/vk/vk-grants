import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';

import * as s from './partners.module.css';
import { Block } from '../../components';
import * as title from './partners-title.module.css';
import PartnerLogo1 from './logos/logo-1.inline.svg';
import PartnerLogo2 from './logos/logo-2.inline.svg';
import PartnerLogo3 from './logos/logo-3.inline.svg';
import PartnerLogo4 from './logos/logo-4.inline.svg';
import PartnerLogo5 from './logos/logo-5.inline.svg';
import PartnerLogo6 from './logos/logo-6.inline.svg';

export const Partners = () => {
    let { t } = useTranslation();
    return (
        <Block>
            <h2 className={title.container}>
                <div className={classNames(title.line1, 'h1-tt')}>{t('partners-block.title.t1')}</div>
                <div className={classNames(title.line2, 'h1-vk')}>{t('partners-block.title.t2')}</div>
                <div className={classNames(title.line3, 'h1-vk')}>{t('partners-block.title.t3')}</div>
            </h2>

            <div className="grid">
                <p className={classNames(s.desc, 'text')}>{t('partners-block.desc')}</p>
            </div>

            <div className={classNames(s.partners, 'grid')}>
                <div className={s.item}>
                    <div className={s.logo}>
                        <PartnerLogo1 className={s.svg} />
                    </div>
                    <p className={'text'}>{t('partners-block.partners.p1')}</p>
                </div>

                <div className={s.item}>
                    <div className={s.logo}>
                        <PartnerLogo2 className={s.svg} />
                    </div>
                    <p className={'text'}>{t('partners-block.partners.p2')}</p>
                </div>

                <div className={s.item}>
                    <div className={s.logo}>
                        <PartnerLogo3 className={s.svg} />
                    </div>
                    <p className={'text'}>{t('partners-block.partners.p3')}</p>
                </div>

                <div className={s.item}>
                    <div className={s.logo}>
                        <PartnerLogo4 className={s.svg} />
                    </div>
                    <p className={'text'}>{t('partners-block.partners.p4')}</p>
                </div>

                <div className={s.item}>
                    <div className={s.logo}>
                        <PartnerLogo5 className={s.svg} />
                    </div>
                    <p className={'text'}>{t('partners-block.partners.p5')}</p>
                </div>

                <div className={s.item}>
                    <div className={s.logo}>
                        <PartnerLogo6 className={s.svg} />
                    </div>
                    <p className={'text'}>{t('partners-block.partners.p6')}</p>
                </div>
            </div>
        </Block>
    );
};
