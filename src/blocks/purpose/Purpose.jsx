import React, { useState } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { StaticImage } from 'gatsby-plugin-image';

import { Block, Pill } from '../../components';
import * as title from './purpose-title.module.css';
import * as desc from './purpose-desc.module.css';
import * as ads from './purpose-ads.module.css';

export const Purpose = () => {
    let { t } = useTranslation();
    const [hovered, setHovered] = useState('carousel');

    return (
        <Block id="purpose">
            <h2 className={title.container}>
                <div className={classNames(title.line1, 'h1-vk')}>{t('purpose-block.title.vk')}</div>
                <div className={classNames(title.line2, 'h1-tt')}>{t('purpose-block.title.tt')}</div>
            </h2>
            <div className="grid">
                <div className={desc.image}>
                    <StaticImage
                        className={desc.img}
                        src="../../images/purpose-block-hero.png"
                        alt="purpose-image"
                        blurredOptions={{ width: 60 }}
                        placeholder="blurred"
                        layout="constrained"
                        height={644}
                        width={796}
                        breakpoints={[376, 796]}
                    />
                </div>

                <div className={desc.text}>
                    <p>{t('purpose-block.desc')}</p>
                </div>
            </div>

            <div className={classNames('grid', 'grid-centered')}>
                <div className={classNames(ads.text, 'h2')}>
                    {t('purpose-block.ads.t1')}
                    <span className={ads.underlined}>{t('purpose-block.ads.t2')}</span>
                    {t('purpose-block.ads.t3')}
                </div>

                <div className={ads.card}>
                    {hovered === 'carousel' && (
                        <StaticImage
                            className={ads.img}
                            src="../../images/purpose-ads-01.png"
                            alt="purpose-ads-01"
                            blurredOptions={{ width: 40 }}
                            placeholder="blurred"
                            layout="constrained"
                            height={334}
                        />
                    )}
                    {hovered === 'post' && (
                        <StaticImage
                            className={ads.img}
                            src="../../images/purpose-ads-02.png"
                            alt="purpose-ads-02"
                            blurredOptions={{ width: 40 }}
                            placeholder="blurred"
                            layout="constrained"
                            height={334}
                        />
                    )}
                    {hovered === 'post-submit' && (
                        <StaticImage
                            className={ads.img}
                            src="../../images/purpose-ads-03.png"
                            alt="purpose-ads-03"
                            blurredOptions={{ width: 40 }}
                            placeholder="blurred"
                            layout="constrained"
                            height={334}
                        />
                    )}
                    {hovered === 'post-submit-video' && (
                        <StaticImage
                            className={ads.img}
                            src="../../images/purpose-ads-04.png"
                            alt="purpose-ads-04"
                            blurredOptions={{ width: 40 }}
                            placeholder="blurred"
                            layout="constrained"
                            height={334}
                        />
                    )}
                    {hovered === 'stories' && (
                        <StaticImage
                            className={ads.img}
                            src="../../images/purpose-ads-05.png"
                            alt="purpose-ads-05"
                            blurredOptions={{ width: 40 }}
                            placeholder="blurred"
                            layout="constrained"
                            height={334}
                        />
                    )}
                    {hovered === 'text-graphics' && (
                        <StaticImage
                            className={ads.img}
                            src="../../images/purpose-ads-06.png"
                            alt="purpose-ads-06"
                            blurredOptions={{ width: 40 }}
                            placeholder="blurred"
                            layout="constrained"
                            height={334}
                        />
                    )}
                    {hovered === 'mini-apps' && (
                        <StaticImage
                            className={ads.img}
                            src="../../images/purpose-ads-07.png"
                            alt="purpose-ads-07"
                            blurredOptions={{ width: 40 }}
                            placeholder="blurred"
                            layout="constrained"
                            height={334}
                        />
                    )}
                </div>
            </div>

            <div className={classNames(ads.pills)}>
                <span className={ads.spacing} />
                <Pill onMouseOver={() => setHovered('carousel')}>{t('purpose-block.pills.p1')}</Pill>
                <Pill onMouseOver={() => setHovered('post')}>{t('purpose-block.pills.p2')}</Pill>
                <Pill onMouseOver={() => setHovered('post-submit')}>{t('purpose-block.pills.p3')}</Pill>
                <Pill onMouseOver={() => setHovered('post-submit-video')}>{t('purpose-block.pills.p4')}</Pill>
                <Pill onMouseOver={() => setHovered('stories')}>{t('purpose-block.pills.p5')}</Pill>
                <Pill onMouseOver={() => setHovered('text-graphics')}>{t('purpose-block.pills.p6')}</Pill>
                <Pill onMouseOver={() => setHovered('mini-apps')}>{t('purpose-block.pills.p7')}</Pill>
                <span className={ads.spacing} />
            </div>
        </Block>
    );
};
