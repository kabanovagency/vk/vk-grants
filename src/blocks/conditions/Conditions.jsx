import classNames from 'classnames';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Block } from '../../components';
import * as s from './conditions.module.css';
import * as title from './conditions-title.module.css';
import I from './numbers/I.inline.svg';
import II from './numbers/II.inline.svg';
import III from './numbers/III.inline.svg';
import IV from './numbers/IV.inline.svg';

export const Conditions = () => {
    let { t } = useTranslation();
    return (
        <Block background="white">
            <h2 className={title.container}>
                <div className={classNames(title.line1, 'h1-tt')}>{t('conditions-block.title.t1')}</div>
                <div className={title.line2}>{t('conditions-block.title.t2')}</div>
            </h2>

            <h3 className={classNames(s.desc, 'h2')}>{t('conditions-block.desc')}</h3>

            <div className={classNames(s.conditions, 'grid')}>
                <div className={s.number} style={{ gridArea: 'I-number' }}>
                    <I />
                </div>
                <div className={classNames(s.text, 'text')} style={{ gridArea: 'I-text' }}>
                    {t('conditions-block.items.c1')}
                </div>

                <div className={s.number} style={{ gridArea: 'II-number' }}>
                    <II />
                </div>
                <div className={classNames(s.text, 'text')} style={{ gridArea: 'II-text' }}>
                    {t('conditions-block.items.c2')}
                </div>

                <div className={s.number} style={{ gridArea: 'III-number' }}>
                    <III />
                </div>
                <div className={classNames(s.text, 'text')} style={{ gridArea: 'III-text' }}>
                    {t('conditions-block.items.c3')}
                </div>

                <div className={s.number} style={{ gridArea: 'IV-number' }}>
                    <IV />
                </div>
                <div className={classNames(s.text, 'text')} style={{ gridArea: 'IV-text' }}>
                    {t('conditions-block.items.c4')}
                </div>
            </div>
        </Block>
    );
};
