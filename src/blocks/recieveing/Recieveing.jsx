import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { StaticImage } from 'gatsby-plugin-image';

import { Block, LinkButton } from '../../components';
import * as title from './recieveing-title.module.css';
import * as desc from './recieveing-desc.module.css';
import * as cards from './recieveing-cards.module.css';

export const Recieveing = () => {
    let { t } = useTranslation();
    return (
        <Block id="recieveing">
            <h2 className={title.container}>
                <h2 className={classNames(title.line1, 'h1-vk')}>{t('recieveing-block.title.t1')}</h2>
                <h2 className={classNames(title.line2, 'h1-vk')}>{t('recieveing-block.title.t2')}</h2>
                <h2 className={classNames(title.line3, 'h1-tt')}>{t('recieveing-block.title.t3')}</h2>
            </h2>

            <div className={classNames(desc.container, 'grid')}>
                <div className={desc.image}>
                    <StaticImage
                        className={desc.img}
                        src="../../images/recieveing-block-hero.png"
                        alt="recieveing-image"
                        blurredOptions={{ width: 60 }}
                        placeholder="blurred"
                        layout="constrained"
                        height={584}
                        width={780}
                        breakpoints={[375, 780]}
                    />
                    <h3 className={classNames(desc.signin, 'h2')}>{t('recieveing-block.application')}</h3>
                </div>

                <div className={desc.info}>
                    <p className={classNames(desc.text, 'text')}>{t('purpose-block.desc')}</p>
                    <LinkButton>{t('recieveing-block.cta')}</LinkButton>
                </div>
            </div>

            <h3 className={classNames(cards.winner, 'h2')}>{t('recieveing-block.winner')}</h3>

            <div className={cards.container}>
                <div className={cards.item}>
                    <div className={cards.image}>
                        <div className={cards.img}>
                            <StaticImage
                                src="../../images/recieveing-block-01.png"
                                alt="recieving-01"
                                blurredOptions={{ width: 60 }}
                                placeholder="blurred"
                                layout="constrained"
                                height={618}
                                width={618}
                                breakpoints={[243, 275, 310, 618]}
                            />
                        </div>
                    </div>
                    <h4 className={classNames('h3', cards.feature)}>{t('recieveing-block.cards.c1.feature')}</h4>
                    <h3 className={classNames('h2', cards.title)}>{t('recieveing-block.cards.c1.title')}</h3>
                    <div className={cards.divider} />
                    <p className={classNames('text', cards.text)}>{t('recieveing-block.cards.c1.text')}</p>
                </div>
                <div className={cards.item}>
                    <div className={cards.image}>
                        <div className={cards.img}>
                            <StaticImage
                                src="../../images/recieveing-block-02.png"
                                alt="recieving-02"
                                blurredOptions={{ width: 60 }}
                                placeholder="blurred"
                                layout="constrained"
                                height={618}
                                width={618}
                                breakpoints={[243, 275, 310, 618]}
                            />
                        </div>
                    </div>
                    <h4 className={classNames('h3', cards.feature)}>{t('recieveing-block.cards.c2.feature')}</h4>
                    <h3 className={classNames('h2', cards.title)}>{t('recieveing-block.cards.c2.title')}</h3>
                    <div className={cards.divider} />
                    <p className={classNames('text', cards.text)}>{t('recieveing-block.cards.c2.text')}</p>
                </div>
                <div className={cards.item}>
                    <div className={cards.image}>
                        <div className={cards.img}>
                            <StaticImage
                                className={cards.img}
                                src="../../images/recieveing-block-03.png"
                                alt="recieving-03"
                                blurredOptions={{ width: 60 }}
                                placeholder="blurred"
                                layout="constrained"
                                height={618}
                                width={618}
                                breakpoints={[243, 275, 310, 618]}
                            />
                        </div>
                    </div>
                    <h4 className={classNames('h3', cards.feature)}>{t('recieveing-block.cards.c3.feature')}</h4>
                    <h3 className={classNames('h2', cards.title)}>{t('recieveing-block.cards.c3.title')}</h3>
                    <div className={cards.divider} />
                    <p className={classNames('text', cards.text)}>{t('recieveing-block.cards.c3.text')}</p>
                </div>
            </div>
        </Block>
    );
};
