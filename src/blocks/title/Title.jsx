import React from 'react';
import { useTranslation } from 'react-i18next';
import { graphql, useStaticQuery } from 'gatsby';
import classNames from 'classnames';

import * as s from './title.module.css';
import { Block, LinkButton } from '../../components';

export const Title = () => {
    let { t } = useTranslation();

    const { background } = useStaticQuery(graphql`
        query {
            background: file(name: { eq: "title-block-background" }) {
                relativePath
                childImageSharp {
                    gatsbyImageData(
                        blurredOptions: { width: 100 }
                        placeholder: BLURRED
                        layout: FULL_WIDTH
                        backgroundColor: "hsl(0, 0%, 15%)"
                    )
                }
            }
        }
    `);

    return (
        <Block bgImage={background}>
            <div className={s.overlay} />
            <div className="grid">
                <h1 className={s.title}>
                    <div className={classNames(s.line1, 'h1-vk')}>{t('title-block.title.grants')}</div>
                    <div className={classNames(s.line2, 'h1-tt')}>{t('title-block.title.project')}</div>
                </h1>

                <p className={classNames(s.desc, 'text')}>{t('title-block.desc')}</p>

                <div className={s.cta}>
                    <LinkButton>{t('title-block.cta')}</LinkButton>
                </div>
            </div>
        </Block>
    );
};
