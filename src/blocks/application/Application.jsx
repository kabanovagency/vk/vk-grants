import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { graphql, useStaticQuery } from 'gatsby';

import { Block, Pill } from '../../components';
import * as s from './application.module.css';
import * as title from './application-title.module.css';

export const Application = () => {
    let { t } = useTranslation();

    const { background } = useStaticQuery(graphql`
        query {
            background: file(name: { eq: "application-block-background" }) {
                relativePath
                childImageSharp {
                    gatsbyImageData(
                        blurredOptions: { width: 100 }
                        placeholder: BLURRED
                        layout: FULL_WIDTH
                        backgroundColor: "hsl(210, 10%, 92%)"
                    )
                }
            }
        }
    `);
    return (
        <Block bgImage={background}>
            <h2 className={title.container}>
                <div className={classNames(title.line1, 'h1-tt')}>{t('application-block.title.t1')}</div>
                <div className={classNames(title.line2)}>
                    <span className={title.arrowed}>{t('application-block.title.t2')}</span>
                </div>
            </h2>

            <div className={s.pills}>
                <div className={s.line}>
                    <span className={s.spacing} />
                    <Pill color="blue">{t('application-block.pills.p1')}</Pill>
                    <Pill color="blue">{t('application-block.pills.p2')}</Pill>
                    <Pill color="blue">{t('application-block.pills.p3')}</Pill>
                    <Pill color="blue">{t('application-block.pills.p4')}</Pill>
                    <Pill color="blue">{t('application-block.pills.p5')}</Pill>
                    <span className={s.spacing} />
                </div>
                <div className={s.line}>
                    <span className={s.spacing} />
                    <Pill color="blue">{t('application-block.pills.p6')}</Pill>
                    <Pill color="blue">{t('application-block.pills.p7')}</Pill>
                    <Pill color="blue">{t('application-block.pills.p8')}</Pill>
                    <Pill color="blue">{t('application-block.pills.p9')}</Pill>
                    <span className={s.spacing} />
                </div>
            </div>
        </Block>
    );
};
