import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { StaticImage } from 'gatsby-plugin-image';

import { Block, LinkButton } from '../../components';
import * as s from './options.module.css';
import * as title from './options-title.module.css';
import * as cards from './options-cards.module.css';

export const Options = () => {
    let { t } = useTranslation();

    return (
        <Block background="gray" id="options">
            <h2 className={title.container}>
                <div className={classNames(title.line1, 'h1-tt')}>{t('options-block.title.t1')}</div>
                <div className={classNames(title.line2, 'h1-vk')}>{t('options-block.title.t2')}</div>
                <div className={classNames(title.line3, 'h1-vk')}>{t('options-block.title.t3')}</div>
            </h2>

            <div className="grid">
                <p className={s.desc}>{t('options-block.desc')}</p>
            </div>

            <div className={classNames(cards.container, 'grid')}>
                <div className={cards.item}>
                    <div className={cards.image}>
                        <div className={cards.img}>
                            <StaticImage
                                src="../../images/options-block-01.png"
                                alt="options-01"
                                blurredOptions={{ width: 60 }}
                                placeholder="blurred"
                                layout="constrained"
                                height={618}
                                width={618}
                                breakpoints={[286, 394, 504, 618]}
                            />
                        </div>
                    </div>
                    <h4 className={classNames('h3', cards.when)}>{t('options-block.cards.c1.when')}</h4>
                    <h3 className={classNames('h2', cards.title)}>{t('options-block.cards.c1.title')}</h3>
                    <p className={classNames('text', cards.text)}>{t('options-block.cards.c1.grants')}</p>
                    <div className={cards.divider} />
                    <p className="text">{t('options-block.cards.c1.signin')}</p>
                </div>

                <div className={cards.item}>
                    <div className={cards.image}>
                        <div className={cards.img}>
                            <StaticImage
                                src="../../images/options-block-02.png"
                                alt="options-02"
                                blurredOptions={{ width: 60 }}
                                placeholder="blurred"
                                layout="constrained"
                                height={618}
                                width={618}
                                breakpoints={[286, 394, 504, 618]}
                            />
                        </div>
                    </div>
                    <h4 className={classNames('h3', cards.when)}>{t('options-block.cards.c2.when')}</h4>
                    <h3 className={classNames('h2', cards.title)}>{t('options-block.cards.c2.title')}</h3>
                    <p className={classNames('text', cards.text)}>{t('options-block.cards.c2.grants')}</p>
                    <div className={cards.divider} />
                    <p className="text">{t('options-block.cards.c2.signin')}</p>
                </div>
            </div>
            <div className="grid">
                <div className={s.cta}>
                    <LinkButton>{t('options-block.cta')}</LinkButton>
                </div>
            </div>
        </Block>
    );
};
