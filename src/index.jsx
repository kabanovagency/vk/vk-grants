import React from 'react';

import { Application, Conditions, Options, Partners, Purpose, Questions, Recieveing, Title } from './blocks';

const VKGrantsLanding = () => {
    return (
        <>
            <Title />
            <Purpose />
            <Options />
            <Application />
            <Conditions />
            <Recieveing />
            <Partners />
            <Questions />
        </>
    );
};

export default VKGrantsLanding;
