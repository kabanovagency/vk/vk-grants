const path = require('path');

module.exports = {
    siteMetadata: {
        title: 'VK Grants',
    },
    plugins: [
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'images',
                path: path.resolve(__dirname, './src/images'),
            },
        },
        'gatsby-plugin-postcss',
        {
            resolve: 'gatsby-plugin-sharp',
            options: {
                defaults: {
                    breakpoints: [960, 1440, 1920],
                },
            },
        },
        {
            resolve: 'gatsby-plugin-react-svg',
            options: {
                rule: {
                    include: /\.inline\.svg$/,
                },
            },
        },
        'gatsby-transformer-sharp',
        'gatsby-plugin-image',
    ],
};
