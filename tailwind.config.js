const breakpoint = (size, start, end) => ({
    [`${size}-al`]: { max: `${end}px` },
    [size]: { min: `${start}px`, max: `${end}px` },
    [`${size}-ag`]: { min: `${start}px` },
});

module.exports = {
    purge: ['./src/**/*.{js,jsx,ts,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        screens: {
            xs: { max: '375px' },
            ...breakpoint('s', 376, 960),
            ...breakpoint('m', 961, 1439),
            ...breakpoint('l', 1440, 1919),
            xl: { min: '1920px' },
        },
        fontFamily: {
            sf: [
                '-apple-system',
                'system-ui',
                'BlinkMacSystemFont',
                'Segoe UI',
                'Roboto',
                'Helvetica Neue',
                'Arial',
                'sans-serif',
            ],
            vk: ['VK Sans Display'],
            tt: ['TT Barrels'],
        },
        colors: {
            'vk-blue': 'hsla(212, 100%, 50%)',
            background: {
                gray: 'hsl(0, 0%, 19%)',
                black: 'hsl(0, 0%, 15%)',
                white: 'hsl(210, 10%, 92%)',
            },
            text: {
                white: 'hsl(0, 0%, 100%)',
                primary: 'hsl(0, 0%, 78%)',
            },
        },
        variables: {
            'h1-vk': '500 104px / 90px',
            'h1-tt': '600 italic 112px / 90px',
            'h1-vk-mobile': '500 52px / 46px',
            'h1-tt-mobile': '600 italic 56px / 46px',
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
